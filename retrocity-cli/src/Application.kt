package site.retrocity.cli

import kotlinx.coroutines.*
import org.koin.core.context.KoinContextHandler
import org.koin.core.context.startKoin
import org.koin.dsl.module
import site.retrocity.client.Client
import site.retrocity.client.ClientProperties
import site.retrocity.common.ConnectedSession
import site.retrocity.common.ConnectedSessionListener
import site.retrocity.common.RetrospectiveCommand
import site.retrocity.common.RetrospectiveUpdate
import site.retrocity.common.model.*
import kotlin.properties.Delegates
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    startKoin {
        modules(retroCityCliModule(args))
    }

    val retroRunner = KoinContextHandler.get().get<RetrospectiveCliRunner>()

    GlobalScope.launch {
        withContext(Dispatchers.IO) {
            do {
                val cmd = readLine()
                if(cmd == null)
                    break
                else
                    retroRunner.onInput(cmd)
            } while (true)
        }
    }

    retroRunner.start()

}

fun retroCityCliModule(args: Array<String>) = module {

    val username = args[0]
    val host = args[1]
    val port = args[2].toInt()
    val team = args[3]
    val iteration = args[4]

    val console = System.console()?.let { JavaConsole(it) } ?: StreamAdapter(System.`in`, System.out)

    val password = System.getenv("password") ?: console.readPassword("Password:")

    single { console }

    single { ClientProperties(username = username, password = password, host = host, port = port) }
    single { Client(get()) }

    single { RetrospectiveCliRunner(get(), get(), team = team.toInt(), iteration = iteration) }
}

class RetrospectiveCliRunner(
    private val client: Client,
    private val console: Console,
    team: Int,
    iteration: String
) {

    val key = RetrospectiveKey(team, iteration)
    lateinit var retro: LiveRetrospective

    fun start() {
        runBlocking {
            client.connectToRetro(key) { session ->
                retro = LiveRetrospective(client, key, console, session)

                object : ConnectedSessionListener<RetrospectiveUpdate> {
                    override fun onMessage(data: RetrospectiveUpdate) {
                        doRetrospectiveUpdate(data, session)
                    }

                    override fun onSessionClosed() {
                        retro.onClose()
                    }

                }
            }
        }
    }

    private fun doRetrospectiveUpdate(data: RetrospectiveUpdate, session: ConnectedSession<RetrospectiveCommand>) {
        runBlocking {
            retro.onRetroUpdate(data)
        }
    }

    fun onInput(command: String) {
        GlobalScope.launch { retro.onInput(command) }
    }
}

class LiveRetrospective(
    private val client: Client,
    private val key: RetrospectiveKey,
    private val console: Console,
    private val session: ConnectedSession<RetrospectiveCommand>) {

    var lastState: RetrospectiveUpdate? = null
    var onInput: suspend (String) -> Unit = { }

    suspend fun onRetroUpdate(newState: RetrospectiveUpdate) {
        val previousStatus = lastState
        lastState = newState

        if (previousStatus == null || previousStatus.state != newState.state) {
            setupState(newState)
        }
    }

    private suspend fun setupState(state: RetrospectiveUpdate) {
        onInput = { }
        when (state.state) {
            RetroState.RETRO_INIT ->
                console.printf("Please wait until this retrospective session begins.\n")
            RetroState.RETRO_STARTED -> {
                console.printf("Participants are gathering, waiting for ${state.owner?.user} to begin the session.\n")
                waitForNextStep(prompt = false)
            }
            RetroState.SPRINT_EVALUATION -> SprintEvaluation().promptForSprintSummary()
            RetroState.SPRINT_VOTING -> SprintVoting().startVoting()
            RetroState.SPRINT_REVIEW -> SprintReview().review()
            else -> {
                console.printf("TODO: step " + state.state + " is not implemented in the client\n")
                waitForNextStep(false)
            }
        }
    }


    private suspend fun waitForNextStep(prompt: Boolean = true) {
        session.send(RetrospectiveCommand.SUBMIT)

        if (client.user == lastState?.owner?.user) {
            console.printf("Type \"next\" to advance to the next step:\n")
            onInput = { if (it == "next") session.send(RetrospectiveCommand.NEXT_STEP) }
        } else {
            if (prompt)
                console.printf("Your answers are submitted.  Please wait for the others!\n")
            onInput = { }
        }
    }

    fun onClose() {
        console.printf("Connection closing!")
        exitProcess(0)
    }

    interface SprintStep

    private inner class SprintEvaluation : SprintStep {

        fun promptForSprintSummary() {
            console.printf("Please provide a brief summary for this last sprint: ")
            onInput = this::postSprintDescription
        }

        suspend fun postSprintDescription(input: String) {
            client.postSprintDescription(key, DescriptionRequest(input))

            // prompt for each rating
            val metrics = client.getRetrospectiveView(key).metrics
            promptForMetric(metrics, 0)
        }

        fun promptForMetric(metrics: List<String>, index: Int) {
            console.printf("How would you rate (1-10) the team this spring for ${metrics[index]}? ")
            onInput = { input -> postSprintRating(metrics, index, input) }
        }

        suspend fun postSprintRating(metrics: List<String>, index: Int, input: String) {
            val rating = input.toRating()
            if (rating == null) {
                console.printf("That input was not valid.  Please try again.\n")
                return
            }

            client.postSprintRating(key, index, RatingRequest(rating))
            if (index < metrics.size - 1)
                promptForMetric(metrics, index + 1)
            else
                waitForNextStep()
        }
    }

    private inner class SprintVoting {

        private lateinit var votes: Array<Int>
        private lateinit var descriptionIndex: List<Int>
        private var count by Delegates.notNull<Int>()

        suspend fun startVoting() {
            val descriptions = client.getListOfSprintDescriptions(key)
            count = descriptions.size
            votes = Array(count) { 0 }
            descriptionIndex = (0 until count).shuffled()

            descriptionIndex.forEachIndexed { idx, ptr ->
                console.printf("%s. %s\n", idx + 1, descriptions[descriptionIndex[ptr]].description)
            }

            promptForVoting()
        }

        fun promptForVoting() {
            val votesLeft = 5 - votes.sum()
            console.printf("Enter numbers followed spaces for $votesLeft vote(s), 'done' when finished, 'restart' to clear votes: ")
            onInput = this::onInput
        }

        suspend fun onInput(input: String) {
            when {
                input == "done" -> {
                    client.postSprintDescriptionVotes(key, VoteRequest(votes.toList()))
                    waitForNextStep()
                }
                input == "restart" -> {
                    votes.fill(0, 0, count)
                }
                input.isNotEmpty() -> {
                    try {
                        val inputs = input.split(' ').filter { it.isNotBlank() }.map { it.toInt() }
                        if (votes.sum() + inputs.size > 5)
                            console.printf("Too many votes")
                        else if (inputs.min()!! < 1 || inputs.max()!! > count)
                            console.printf("You entered an invalid number")
                        else {
                            inputs.forEach { votes[descriptionIndex[it-1]]++ }
                            promptForVoting()
                        }
                    } catch (e: NumberFormatException) {
                        console.printf("Your input was not valid")
                    }
                }
            }
        }
    }

    private inner class SprintReview {

        suspend fun review() {
            console.printf("Sprint ratings:\n")

            client.getListOfSprintRatings(key).metrics.forEachIndexed { idx, it ->
                console.printf("${idx + 1}. ${it.title}  min: ${it.min} avg: ${it.avg} max: ${it.max}\n")
            }

            console.printf("\nSprint Descriptions:\n")
            client.getListOfSprintDescriptions(key).sortedByDescending { it.votes }.forEachIndexed { idx, it ->
                console.printf("${idx + 1}. ${it.description}  votes: ${it.votes}\n")
            }


            waitForNextStep(prompt = false)
        }
    }
}

private fun String.toRating(): Int? = try {
    toInt().let { if (it in 1..10) it else null }
} catch (e: NumberFormatException) {
    null
}