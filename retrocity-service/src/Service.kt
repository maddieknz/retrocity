@file:OptIn(KlockExperimental::class)

package site.retrocity.service

import com.soywiz.klock.DateTime
import com.soywiz.klock.annotations.KlockExperimental
import com.soywiz.klock.wrapped.wrapped
import io.ktor.features.*
import site.retrocity.common.model.*
import site.retrocity.model.*

class IterationService(private val repository: IterationRepository) {

    fun list(): List<Iteration> = repository.items.toList()

    fun create(request: Iteration): Iteration = repository.save(request)

}

class TeamService(
    private val repository: TeamRepository,
    private val sequenceProvider: SequenceProvider
) {

    fun list(): List<Team> = repository.items.toList()

    fun create(request: CreateTeamRequest, owner: String): Team {
        val team = Team(
            id = sequenceProvider.nextValue(Seq.TEAM),
            name = request.name,
            owner = owner,
            members = request.members.toSet(),
            metrics = request.metrics.toSet(),
            observationGroups = request.observationGroups
        )
        repository.save(team)
        return team
    }
}

class RetrospectiveService(
    private val repository: RetrospectiveRepository,
    private val teamRepository: TeamRepository,
    private val actionItemRepository: ActionItemRepository,
    private val iterationRepository: IterationRepository,
    private val sequenceProvider: SequenceProvider
) {

    fun list(): List<Retrospective> = repository.items.toList()

    fun create(request: CreateRetrospectiveRequest, owner: String): CreateRetrospectiveResponse {
        val team = teamRepository.find(request.team) ?: throw NotFoundException() // todo
        val iteration = iterationRepository.find(request.iteration) ?: throw NotFoundException() // todo

        val retrospective = Retrospective(
            team = request.team,
            owner = owner,
            iteration = iteration,
            state = RetroState.RETRO_INIT,
            stateStart = DateTime.now().wrapped,
            participants = team.members.toSet(),
            actionItems = listOf(),
            sprintDescription = mutableMapOf(),
            sprintRatings = team.metrics.map { SprintRating(metric = it, ratings = mutableMapOf()) },
            observationGroups = team.observationGroups,
            observations = mutableListOf()
        )

        repository.save(retrospective)
        return retrospective.toCreateRetrospectiveResponse()
    }

    fun view(key: RetrospectiveKey): ViewRetrospectiveResponse {
        val it = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        return it.toViewRetrospectiveResponse()
    }

    fun listSprintDescriptions(key: RetrospectiveKey): List<SprintDescriptionResponse> {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        return retrospective.toListSpringDescriptionsResponse()
    }

    fun postSprintDescription(key: RetrospectiveKey, user: String, body: DescriptionRequest) {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        retrospective.sprintDescription[user] = SprintDescription(body.description)
        repository.save(retrospective)
    }

    fun postSprintDescriptionVotes(key: RetrospectiveKey, user: String, body: VoteRequest) {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        assert(retrospective.sprintDescription.size == body.votes.size)
        assert(body.votes.sum() <= 5)
        retrospective.sprintDescription.values.forEachIndexed { id, it ->
            it.votes[user] = body.votes[id]
        }
        repository.save(retrospective)
    }

    fun postSprintRating(key: RetrospectiveKey, user: String, metric: Int, body: RatingRequest) {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        retrospective.sprintRatings[metric].ratings[user] = body.rating
        repository.save(retrospective)
    }

    fun listSprintRatings(key: RetrospectiveKey): ListSprintRatingsResponse {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        return retrospective.toListSpringRatingsResponse()
    }

    fun postObservation(key: RetrospectiveKey, user: String,
                        body: CreateObservationRequest): CreateObservationResponse {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        val observation = Observation(
            submittedBy = user,
            group = body.group,
            text = body.text,
            deleted = false,
            selected = false,
            votes = mutableMapOf(),
            actionItems = mutableListOf()
        )
        retrospective.observations.add(observation)
        repository.save(retrospective)
        return CreateObservationResponse(index = retrospective.observations.size - 1)
    }

    fun postObservationVotes(key: RetrospectiveKey, user: String, body: VotesRequest) {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()

        val observationsByGroup = retrospective.observations.mapIndexed { idx, it -> Pair(idx, it.group) }.toMap()

        retrospective.observationGroups.forEachIndexed { idx, grp ->
            val votes: Int = body.votes
                .filter { observationsByGroup[it.key] == idx }
                .map { it.value }
                .sum()
            assert(votes <= grp.votesPerUser)
        }

        retrospective.observations.forEachIndexed { idx, it ->
            it.votes[user] = body.votes[idx] ?: 0
        }

        repository.save(retrospective)
    }

    fun updateObservation(key: ObservationKey, user: String, body: UpdateObservationRequest) {
        val (retrospective, observation) = findObservation(key)
        assert(observation.submittedBy == user || retrospective.owner == user)
        body.text?.let { observation.text = it }
        body.selected?.let { observation.selected = it }
        repository.save(retrospective)
    }

    fun deleteObservation(key: ObservationKey, user: String) {
        val (retrospective, observation) = findObservation(key)
        observation.deleted = true
        assert(observation.submittedBy == user || retrospective.owner == user)
        repository.save(retrospective)
    }

    fun createActionItem(key: ObservationKey, user: String, body: CreateActionItemRequest): CreateActionItemResponse {
        val (retrospective, observation) = findObservation(key)
        val actionItem = ActionItem(
            id = sequenceProvider.nextValue(Seq.ACTION_ITEM),
            submittedBy = user,
            observationKey = key,
            observationText = observation.text,
            text = body.text,
            deleted = false,
            ratings = mutableMapOf(),
            votes = mutableMapOf(),
            volunteers = mutableSetOf()
        )
        actionItemRepository.save(actionItem)
        observation.actionItems.add(actionItem.id)
        repository.save(retrospective)
        return CreateActionItemResponse(actionItem.id)
    }

    fun updateActionItem(key: ActionItemKey, user: String, body: UpdateActionItemRequest) {
        val (retrospective, _, actionItem) = findActionItem(key)
        body.text?.let { actionItem.text = it }
        body.duration?.let { actionItem.duration = it }
        repository.save(retrospective)
    }

    fun deleteActionItem(key: ActionItemKey, user: String) {
        val (retrospective, observation, actionItem) = findActionItem(key)
        actionItem.deleted = true
        observation.actionItems.remove(actionItem.id)
        repository.save(retrospective)
    }

    fun postActionItemVotes(key: ObservationGroupKey, user: String, body: VotesRequest) {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        val observations = retrospective.observations.filter { it.group == key.group }
        if (observations.isEmpty()) throw NotFoundException()
        val actionItemIds = observations.flatMap { it.actionItems }
        val actionItems = actionItemRepository.find(actionItemIds)
        actionItems.forEach { actionItem ->
            actionItem.votes[user] = body.votes[actionItem.id] ?: 0
            actionItemRepository.save(actionItem)
        }
    }

    private fun findObservation(key: ObservationKey): Pair<Retrospective, Observation> {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        val observation = retrospective.observations[key.observation]
        return Pair(retrospective, observation)
    }

    private fun findActionItem(key: ActionItemKey): Triple<Retrospective, Observation, ActionItem> {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        val observation = retrospective.observations[key.observation]
        val actionItem = actionItemRepository.find(observation.actionItems[key.actionItem])
            ?: throw NotFoundException()
        return Triple(retrospective, observation, actionItem)
    }

    fun listObservations(key: RetrospectiveKey): ListObservationResult {
        val retrospective = repository.find(key.team, key.iteration) ?: throw NotFoundException()
        val actionItemIds = retrospective.observations.flatMap { it.actionItems }
        val actionItems = actionItemRepository.find(actionItemIds).associateBy { it.id }
        return retrospective.toListObservationResults(actionItems)
    }
}
