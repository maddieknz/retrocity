@file:OptIn(KlockExperimental::class)

package site.retrocity.service

import com.soywiz.klock.DateTime
import com.soywiz.klock.annotations.KlockExperimental
import com.soywiz.klock.wrapped.WDateTime
import io.ktor.features.*
import kotlinx.atomicfu.locks.withLock
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import site.retrocity.common.ConnectedSession
import site.retrocity.common.ConnectedSessionListener
import site.retrocity.common.RetrospectiveCommand
import site.retrocity.common.RetrospectiveUpdate
import site.retrocity.common.model.RetroState
import site.retrocity.common.model.RetrospectiveKey
import site.retrocity.model.RetrospectiveRepository
import java.util.concurrent.locks.ReentrantLock

typealias RetrospectiveSession = ConnectedSession<RetrospectiveUpdate>

data class LiveRetrospective(
    val key: RetrospectiveKey,
    val owner: String,
    val participants: Set<String>,
    val sessions: MutableMap<String, RetrospectiveSession> = mutableMapOf(),

    // State info
    var state: RetroState,
    var stateStart: WDateTime,
    val stepCompletedBy: MutableSet<String> = mutableSetOf()
) {

    val update: RetrospectiveUpdate
        get() = RetrospectiveUpdate(
            state = this.state,
            timer = (DateTime.now() - stateStart.value).seconds.toLong(),
            participants = this.participants.map { user ->
                RetrospectiveUpdate.Participant(
                        user = user,
                        online = sessions.containsKey(user),
                        completedStep = stepCompletedBy.contains(user),
                        owner = user == owner
                )
            }
        )
}

private val states = RetroState.values()

class LiveRetrospectiveService(private val retrospectiveRepository: RetrospectiveRepository,
                               private val async: CoroutineScope = GlobalScope) {

    val lock = ReentrantLock()
    val retrospectives = mutableMapOf<RetrospectiveKey, LiveRetrospective>()

    fun handleSession(
        key: RetrospectiveKey,
        session: RetrospectiveSession
    ): ConnectedSessionListener<RetrospectiveCommand> {
        handleOpenedSession(key, session)

        return object : ConnectedSessionListener<RetrospectiveCommand> {

            override fun onMessage(data: RetrospectiveCommand) = handleCommand(key, session, data)

            override fun onSessionClosed() = handleClosedSession(key, session)
        }
    }

    private fun handleOpenedSession(key: RetrospectiveKey, session: ConnectedSession<RetrospectiveUpdate>) =
        with(key) {
            it.sessions[session.user] = session

            // When the host joins the retro, automatically flip from RETRO_INIT to RETRO_STARTED
            if (it.state == RetroState.RETRO_INIT && it.owner == session.user) {
                it.state = RetroState.RETRO_STARTED
            }

            updateSessions(it)
        }


    private fun handleClosedSession(key: RetrospectiveKey, session: ConnectedSession<RetrospectiveUpdate>) =
        with(key) {
            it.sessions.remove(session.user)
            updateSessions(it)
        }

    private fun handleCommand(key: RetrospectiveKey, session: RetrospectiveSession, cmd: RetrospectiveCommand): Unit =
        with(key) {
            when (cmd) {
                RetrospectiveCommand.NEXT_STEP -> handleNextStep(it, session)
                RetrospectiveCommand.PREVIOUS_STEP -> handlePreviousStep(it, session)
                RetrospectiveCommand.SUBMIT -> handleSubmit(it, session)
                RetrospectiveCommand.STOP_RETRO -> handleStopRetro(it, session)
            }
        }

    private fun handleNextStep(live: LiveRetrospective, session: RetrospectiveSession) {
        live.state = states[live.state.ordinal + 1]
        live.stateStart = WDateTime.now()
        updateRetro(live)
        updateSessions(live)
    }

    private fun handlePreviousStep(live: LiveRetrospective, session: RetrospectiveSession) {
        live.state = states[live.state.ordinal - 1]
        live.stateStart = WDateTime.now()
        updateRetro(live)
        updateSessions(live)
    }

    private fun handleSubmit(live: LiveRetrospective, session: RetrospectiveSession) {
        live.stepCompletedBy.add(session.user)
        updateSessions(live)
    }

    private fun handleStopRetro(live: LiveRetrospective, session: RetrospectiveSession) {
        async.launch {
            live.sessions.values.forEach { it.close() }
        }

        live.sessions.clear()
    }

    private fun fetch(key: RetrospectiveKey): LiveRetrospective =
        retrospectives.computeIfAbsent(key) {
            retrospectiveRepository.find(key.team, key.iteration)?.let {
                LiveRetrospective(
                    key = key,
                    owner = it.owner,
                    state = it.state,
                    stateStart = it.stateStart,
                    participants = it.participants
                )
            } ?: throw NotFoundException()
        }

    private fun <T> with(key: RetrospectiveKey, block: (LiveRetrospective) -> T): T =
        lock.withLock { block(fetch(key)) }

    private fun updateRetro(live: LiveRetrospective) {
        retrospectiveRepository.find(live.key.team, live.key.iteration)?.also {
            it.state = live.state
            it.stateStart = live.stateStart
            retrospectiveRepository.save(it)
        } ?: throw NotFoundException()
    }

    private fun updateSessions(live: LiveRetrospective) {
        val update = live.update
        val sessions = live.sessions.values
        async.launch {
            sessions.forEach { it.send(update) }
        }
    }
}

