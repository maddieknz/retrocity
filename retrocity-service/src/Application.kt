package site.retrocity

import com.mongodb.client.MongoClient
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.serialization.*
import io.ktor.websocket.*
import org.koin.core.context.startKoin
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.ktor.ext.inject
import org.litote.kmongo.KMongo
import site.retrocity.model.*
import site.retrocity.routes.iterationRoutes
import site.retrocity.routes.retrospectiveRoutes
import site.retrocity.routes.teamRoutes
import site.retrocity.security.ISecurityService
import site.retrocity.security.TestSecurityService
import site.retrocity.service.IterationService
import site.retrocity.service.LiveRetrospectiveService
import site.retrocity.service.RetrospectiveService
import site.retrocity.service.TeamService

fun main(args: Array<String>) {
    startKoin {
        modules(RetroCityModule)
    }

    io.ktor.server.netty.EngineMain.main(args)
}

val RetroCityModule = module {

    // database
    single { KMongo.createClient("mongodb://retrocity:test@localhost/retrocity") }
    single { get<MongoClient>().getDatabase("retrocity") }

    // repositories
    single { IterationRepository(get()) }
    single { TeamRepository(get()) }
    single { RetrospectiveRepository(get()) }
    single { ActionItemRepository(get()) }
    single { SequenceProvider(get()) }

    // services
    single { TestSecurityService() } bind ISecurityService::class
    single { IterationService(get()) }
    single { TeamService(get(), get()) }
    single { RetrospectiveService(get(), get(), get(), get(), get()) }
    single { LiveRetrospectiveService(get()) }
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    val securityService: ISecurityService by inject()

    install(Authentication) {
        basic {
            validate { securityService.authenticate(it) }
        }
    }

    install(ContentNegotiation) {
        json()
    }

    install(WebSockets)

    routing {
        authenticate {
            get("/") {
                call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
            }

            iterationRoutes()
            teamRoutes()
            retrospectiveRoutes()

        }

        static("/static") {
            resources("static")
        }
    }
}
