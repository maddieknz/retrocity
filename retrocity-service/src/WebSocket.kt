package site.retrocity

import io.ktor.auth.*
import io.ktor.http.cio.websocket.*
import io.ktor.websocket.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import site.retrocity.common.ConnectedSession
import site.retrocity.common.ConnectedSessionListener
import site.retrocity.ext.username


class WebSocketAdapter<IN : Any, OUT : Any>(
    private val inSerializer: KSerializer<IN>,
    private val outSerializer: KSerializer<OUT>,
    private val jsonConfig: JsonConfiguration = JsonConfiguration.Stable
) {

    val json = Json(jsonConfig)

    suspend fun connect(
        session: WebSocketServerSession,
        bindingMethod: (ConnectedSession<OUT>) -> ConnectedSessionListener<IN>
    ) {
        val adapter = SessionAdapter(session)
        val listener = bindingMethod(adapter)

        for (frame in session.incoming) {
            if (frame is Frame.Close) break
            if (frame !is Frame.Text) continue
            val data = json.parse(inSerializer, frame.readText())
            listener.onMessage(data)
        }

        listener.onSessionClosed()
    }

    inner class SessionAdapter(
        private val session: WebSocketServerSession
    ) : ConnectedSession<OUT> {

        override val user: String
            get() = session.call.authentication.username

        override suspend fun send(data: OUT) {
            val text = json.stringify(outSerializer, data)
            session.outgoing.send(Frame.Text(text))
        }

        override suspend fun close() {
            session.close()
        }

    }

}