package site.retrocity.model

import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.ReplaceOptions
import com.mongodb.client.model.ReturnDocument
import kotlinx.serialization.Serializable
import org.litote.kmongo.*
import site.retrocity.common.model.ActionItem
import site.retrocity.common.model.Iteration
import site.retrocity.common.model.Retrospective
import site.retrocity.common.model.Team

class IterationRepository(val db: MongoDatabase) {

    val items: Iterable<Iteration>
        get() = db.getCollection<Iteration>().find()

    fun save(it: Iteration): Iteration {
        db.getCollection<Iteration>().replaceOne(
                it::name eq it.name, it,
                ReplaceOptions().upsert(true)
        )
        return it
    }

    fun find(name: String): Iteration? =
            db.getCollection<Iteration>().findOne(Iteration::name eq name)

}

class TeamRepository(val db: MongoDatabase) {

    val items: Iterable<Team>
        get() = db.getCollection<Team>().find()

    fun save(it: Team): Team {
        db.getCollection<Team>().replaceOne(
            it::id eq it.id, it,
            ReplaceOptions().upsert(true)
        )
        return it
    }

    fun find(id: Int): Team? =
        db.getCollection<Team>().findOne(Team::id eq id)
}

class RetrospectiveRepository(val db: MongoDatabase) {

    val items: Iterable<Retrospective>
        get() = db.getCollection<Retrospective>().find()

    fun save(it: Retrospective): Retrospective {
        db.getCollection<Retrospective>().replaceOne(
            and(it::team eq it.team, it::iteration eq it.iteration),
            it,
            ReplaceOptions().upsert(true)
        )
        return it
    }

    fun find(team: Int, iteration: String): Retrospective? =
            db.getCollection<Retrospective>().findOne(
                    Retrospective::team eq team,
                    Retrospective::iteration / Iteration::name eq iteration
            )
}

class ActionItemRepository(val db: MongoDatabase) {

    val items: Iterable<ActionItem>
        get() = db.getCollection<ActionItem>().find()

    fun save(it: ActionItem): ActionItem {
        db.getCollection<ActionItem>().replaceOne(
                it::id eq it.id,
                it,
                ReplaceOptions().upsert(true)
        )
        return it
    }

    fun find(id: Int): ActionItem? =
            db.getCollection<ActionItem>().findOne(ActionItem::id eq id)

    fun find(ids: Iterable<Int>): Iterable<ActionItem> =
            db.getCollection<ActionItem>().find(ActionItem::id `in` ids)
}

@Serializable
private data class Sequence(
        val name: String,
        var value: Int = 0
)

enum class Seq(val dbValue: String) {
    TEAM("name"),
    ACTION_ITEM("actionItem")
}

class SequenceProvider(private val db: MongoDatabase) {

    fun nextValue(type: Seq): Int =
        db.getCollection<Sequence>().findOneAndUpdate(
            Sequence::name eq type.dbValue,
            inc(Sequence::value, 1),
            FindOneAndUpdateOptions().upsert(true).returnDocument(ReturnDocument.AFTER)
        )!!.value
}