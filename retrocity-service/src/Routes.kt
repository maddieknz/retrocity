package site.retrocity.routes

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.websocket.*
import org.koin.ktor.ext.inject
import site.retrocity.WebSocketAdapter
import site.retrocity.common.RetrospectiveCommand
import site.retrocity.common.RetrospectiveUpdate
import site.retrocity.common.model.ActionItemKey
import site.retrocity.common.model.ObservationGroupKey
import site.retrocity.common.model.ObservationKey
import site.retrocity.common.model.RetrospectiveKey
import site.retrocity.ext.username
import site.retrocity.service.IterationService
import site.retrocity.service.LiveRetrospectiveService
import site.retrocity.service.RetrospectiveService
import site.retrocity.service.TeamService

fun Route.iterationRoutes() {
    val service: IterationService by inject()

    get("/iterations") {
        call.respond(service.list())
    }

    post("/iterations") {
        call.respond(HttpStatusCode.Created, service.create(call.receive()))
    }
}

fun Route.teamRoutes() {
    val service: TeamService by inject()

    get("/teams") {
        call.respond(service.list())
    }

    post("/teams") {
        call.respond(
            HttpStatusCode.Created, service.create(
                request = call.receive(), owner = call.authentication.username
            )
        )
    }
}

fun Route.retrospectiveRoutes() {
    val service: RetrospectiveService by inject()
    val liveService: LiveRetrospectiveService by inject()

    val webSocketAdapter = WebSocketAdapter(RetrospectiveCommand.serializer(), RetrospectiveUpdate.serializer())

    get("/retrospectives") {
        call.respond(service.list())
    }

    post("/retrospectives") {
        call.respond(
                HttpStatusCode.Created, service.create(
                request = call.receive(), owner = call.authentication.username)
        )
    }

    route("/retrospectives/{team}/{iteration}") {

        get {
            call.respond(
                    service.view(
                            key = readRetrospectiveKey(call.parameters)
                    )
            )
        }

        webSocket {
            webSocketAdapter.connect(this) {
                liveService.handleSession(readRetrospectiveKey(call.parameters), it)
            }
        }

        get("/descriptions") {
            call.respond(
                    service.listSprintDescriptions(
                            key = readRetrospectiveKey(call.parameters)
                    )
            )
        }

        post("/descriptions") {
            call.respond(
                    HttpStatusCode.Accepted,
                    service.postSprintDescription(
                            key = readRetrospectiveKey(call.parameters),
                            user = call.authentication.username,
                            body = call.receive()
                    )
            )
        }

        post("/descriptions/votes") {
            call.respond(
                    HttpStatusCode.Accepted,
                    service.postSprintDescriptionVotes(
                            key = readRetrospectiveKey(call.parameters),
                            user = call.authentication.username,
                            body = call.receive()
                    )
            )
        }

        get("/ratings") {
            call.respond(
                    service.listSprintRatings(
                            key = readRetrospectiveKey(call.parameters)
                    )
            )
        }

        post("/ratings/{metric}") {
            call.respond(
                    HttpStatusCode.Accepted,
                    service.postSprintRating(
                            key = readRetrospectiveKey(call.parameters),
                            metric = call.parameters["metric"]?.toIntOrNull()
                                    ?: throw BadRequestException("Invalid metric #"),
                            user = call.authentication.username,
                            body = call.receive()
                    )
            )
        }

        route("/observations") {

            post {
                call.respond(
                        HttpStatusCode.Accepted,
                        service.postObservation(
                                key = readRetrospectiveKey(call.parameters),
                                user = call.authentication.username,
                                body = call.receive()
                        )
                )
            }

            get {
                call.respond(
                        service.listObservations(
                                key = readRetrospectiveKey(call.parameters)
                        )
                )
            }

            post("/votes") {
                call.respond(
                        HttpStatusCode.Accepted,
                        service.postObservationVotes(
                                key = readRetrospectiveKey(call.parameters),
                                user = call.authentication.username,
                                body = call.receive()
                        )
                )
            }

            post("/groups/{group}/actions/votes") {
                call.respond(
                        HttpStatusCode.Accepted,
                        service.postActionItemVotes(
                                key = readObservationGroupKey(call.parameters),
                                user = call.authentication.username,
                                body = call.receive()
                        )
                )
            }

            route("/{observation}") {

                patch {
                    call.respond(
                            HttpStatusCode.Accepted,
                            service.updateObservation(
                                    key = readObservationKey(call.parameters),
                                    user = call.authentication.username,
                                    body = call.receive()
                            )
                    )
                }

                delete {
                    call.respond(
                            HttpStatusCode.NoContent,
                            service.deleteObservation(
                                    key = readObservationKey(call.parameters),
                                    user = call.authentication.username
                            )
                    )
                }

                route("/actions") {

                    post {
                        call.respond(
                                HttpStatusCode.Created,
                                service.createActionItem(
                                        key = readObservationKey(call.parameters),
                                        user = call.authentication.username,
                                        body = call.receive()
                                )
                        )
                    }

                    route("/{action}") {

                        patch {
                            call.respond(
                                    HttpStatusCode.NoContent,
                                    service.updateActionItem(
                                            key = readActionItemKey(call.parameters),
                                            user = call.authentication.username,
                                            body = call.receive()
                                    )
                            )
                        }

                        delete {
                            call.respond(
                                    HttpStatusCode.NoContent,
                                    service.deleteActionItem(
                                            key = readActionItemKey(call.parameters),
                                            user = call.authentication.username
                                    )
                            )
                        }
                    }
                }
            }
        }
    }


}

private fun readObservationKey(parameters: Parameters) =
        ObservationKey(
                team = parameters["team"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid team"),
                iteration = parameters["iteration"] ?: "",
                observation = parameters["observation"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid observation #")
        )

private fun readObservationGroupKey(parameters: Parameters) =
        ObservationGroupKey(
                team = parameters["team"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid team"),
                iteration = parameters["iteration"] ?: "",
                group = parameters["group"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid group #")
        )


private fun readActionItemKey(parameters: Parameters) =
        ActionItemKey(
                team = parameters["team"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid team"),
                iteration = parameters["iteration"] ?: "",
                observation = parameters["observation"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid observation #"),
                actionItem = parameters["action"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid action item #")
        )

private fun readRetrospectiveKey(parameters: Parameters) =
        RetrospectiveKey(
                team = parameters["team"]?.toIntOrNull()
                        ?: throw BadRequestException("Invalid team"),
                iteration = parameters["iteration"] ?: ""
        )