package site.retrocity

import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.server.testing.*
import io.mockk.every
import io.mockk.mockkClass
import org.junit.Rule
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import site.retrocity.security.ISecurityService
import site.retrocity.security.RetrocityPrincipal
import java.nio.charset.Charset
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest : KoinTest {

    @get:Rule
    val mockProvider = MockProviderRule.create { mockkClass(it) }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(RetroCityModule)
    }

    @Test
    fun testRoot() {
        declareMock<ISecurityService> {
            every { authenticate(any()) } returns RetrocityPrincipal("john")
        }

        withTestApplication({ module(testing = true) }) {
            handleRequestWithBasic("/", "john", "test").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("HELLO WORLD!", response.content)
            }
        }
    }

    private fun TestApplicationEngine.handleRequestWithBasic(
        url: String, user: String, pass: String, charset: Charset = Charsets.ISO_8859_1
    ) =
        handleRequest {
            uri = url
            val encoded = "$user:$pass".toByteArray(charset).encodeBase64()
            addHeader(HttpHeaders.Authorization, "Basic $encoded")
        }

}

fun ByteArray.encodeBase64(): String = Base64.getEncoder().encodeToString(this)
