package site.retrocity

import com.soywiz.klock.DateTime
import com.soywiz.klock.annotations.KlockExperimental
import com.soywiz.klock.wrapped.WDateTime
import com.soywiz.klock.wrapped.wrapped
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import site.retrocity.common.RetrospectiveCommand
import site.retrocity.common.RetrospectiveUpdate
import site.retrocity.common.model.Iteration
import site.retrocity.common.model.RetroState
import site.retrocity.common.model.Retrospective
import site.retrocity.common.model.RetrospectiveKey
import site.retrocity.model.RetrospectiveRepository
import site.retrocity.service.LiveRetrospectiveService
import site.retrocity.service.RetrospectiveSession
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SessionTest {

    val retrospectiveRepository = mockk<RetrospectiveRepository>()

    @OptIn(KlockExperimental::class)
    @Test
    fun `submit command adds user to submitted list`() {
        val user = "johndoe"
        val retroKey = RetrospectiveKey(1, "2020.Q1.1")
        val session = TestSession(user)

        val retrospective = Retrospective(
            team = retroKey.team,
            iteration = Iteration(retroKey.iteration, DateTime.now().date.wrapped),
            state = RetroState.RETRO_INIT,
            stateStart = WDateTime.now(),
            actionItems = listOf(),
            participants = setOf(user),
            observations = mutableListOf(),
            observationGroups = listOf(),
            owner = user,
            sprintDescription = mutableMapOf(),
            sprintRatings = listOf()
        )

        every { retrospectiveRepository.find(retroKey.team, retroKey.iteration) } returns retrospective

        runBlocking {

            val sut = LiveRetrospectiveService(retrospectiveRepository, this)

            sut.handleSession(retroKey, session).apply {
                onMessage(RetrospectiveCommand.SUBMIT)
            }
        }

        assertEquals(session.updates.size, 2, "Session has received 2 updates")
        val update = session.updates[1]
        assertEquals(update.participants.size, 1, "Retro update shows 1 participant")
        assertEquals(update.participants[0].user, user, "Participant is our user")
        assertTrue(update.participants[0].online, "Participant shows online")
        assertTrue(update.participants[0].completedStep, "Participant has completed the step")
    }

    class TestSession(override val user: String) : RetrospectiveSession {
        var updates = mutableListOf<RetrospectiveUpdate>()
        var isOpen = true

        override suspend fun send(data: RetrospectiveUpdate) {
            updates.add(data)
        }

        override suspend fun close() {
            isOpen = false
        }
    }
}