@file:OptIn(KtorExperimentalAPI::class)

package site.retrocity.client

import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.auth.*
import io.ktor.client.features.auth.providers.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.util.*
import site.retrocity.common.ConnectedSession
import site.retrocity.common.ConnectedSessionListener
import site.retrocity.common.RetrospectiveCommand
import site.retrocity.common.RetrospectiveUpdate
import site.retrocity.common.model.*

data class ClientProperties(
    val username: String,
    val password: String,
    val host: String,
    val port: Int
)

class Client(val properties: ClientProperties) {

    val user: String get() = properties.username

    val httpClient = HttpClient {

        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }

        install(Auth) {
            basic {
                this.username = properties.username
                this.password = properties.password
            }
        }

        install(WebSockets)

        defaultRequest {
            host = properties.host
            port = properties.port
        }
    }

    suspend fun getAllIterations(): List<Iteration> =
            httpClient.get {
                url { path("iterations") }
            }

    suspend fun postIteration(request: Iteration): Unit =
            httpClient.post {
                url { path("iterations") }
                body = request
            }

    suspend fun getAllTeams(): List<Team> =
            httpClient.get {
                url { path("teams") }
            }

    suspend fun postTeam(request: CreateTeamRequest): Unit =
            httpClient.post {
                url { path("teams") }
                body = request
            }

    val wsAdapter = WebSocketAdapter(RetrospectiveUpdate.serializer(), RetrospectiveCommand.serializer())

    suspend fun connectToRetro(
            key: RetrospectiveKey,
            bindingMethod: (ConnectedSession<RetrospectiveCommand>) -> ConnectedSessionListener<RetrospectiveUpdate>
    ) {
        httpClient.ws(request = {
            url { path("retrospectives", key.team.toString(), key.iteration) }
        }) { wsAdapter.connect(this, properties.username, bindingMethod) }
    }

    suspend fun getListOfRetrospectives(): List<Retrospective> =
            httpClient.get {
                url { path("retrospectives") }
            }

    suspend fun postRetrospective(request: CreateRetrospectiveRequest): Unit =
            httpClient.post {
                url { path("retrospectives") }
                body = request
            }

    suspend fun getRetrospectiveView(key: RetrospectiveKey): ViewRetrospectiveResponse =
            httpClient.get {
                url { path("retrospectives", key.team.toString(), key.iteration) }
            }

    suspend fun getListOfSprintDescriptions(key: RetrospectiveKey): List<SprintDescriptionResponse> =
            httpClient.get {
                url { path("retrospectives", key.team.toString(), key.iteration, "descriptions") }
            }

    suspend fun postSprintDescription(
            key: RetrospectiveKey,
            request: DescriptionRequest): Unit =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "descriptions") }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun postSprintDescriptionVotes(
            key: RetrospectiveKey,
            request: VoteRequest): Unit =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "descriptions", "votes") }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun getListOfSprintRatings(key: RetrospectiveKey): ListSprintRatingsResponse =
            httpClient.get {
                url { path("retrospectives", key.team.toString(), key.iteration, "ratings") }
            }

    suspend fun postSprintRating(
            key: RetrospectiveKey,
            metric: Int,
            request: RatingRequest): Unit =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "ratings", metric.toString()) }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun postObservation(
            key: RetrospectiveKey,
            request: CreateObservationRequest): CreateObservationResponse =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations") }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun getListOfObservations(key: RetrospectiveKey): ListObservationResult =
            httpClient.get {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations") }
            }

    suspend fun postObservationVotes(
            key: RetrospectiveKey,
            request: VotesRequest): Unit =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", "votes") }
                body = request
            }

    suspend fun postActionItemVotes(
            key: ObservationGroupKey,
            request: VotesRequest): Unit =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", "groups", key.group.toString(), "actions", "votes") }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun patchUpdateObservation(
            key: ObservationGroupKey,
            request: UpdateObservationRequest): Unit =
            httpClient.patch {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", key.group.toString()) }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun deleteObservation(key: ObservationKey): Unit =
            httpClient.delete {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", key.observation.toString()) }
            }

    suspend fun postCreateActionItem(
            key: ObservationKey,
            request: CreateActionItemRequest): CreateActionItemResponse =
            httpClient.post {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", key.observation.toString(), "actions") }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun patchUpdateActionItem(
            key: ActionItemKey,
            request: UpdateActionItemRequest): Unit =
            httpClient.patch {
                url { path("retrospectives", key.team.toString(), key.iteration,  "observations", key.observation.toString(), key.actionItem.toString()) }
                contentType(ContentType.Application.Json)
                body = request
            }

    suspend fun deleteActionItem(key: ActionItemKey): Unit =
            httpClient.delete {
                url { path("retrospectives", key.team.toString(), key.iteration, "observations", key.observation.toString(), key.actionItem.toString()) }
            }
}