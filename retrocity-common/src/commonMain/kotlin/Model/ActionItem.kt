package site.retrocity.common.model

import kotlinx.serialization.Serializable

@Serializable
data class ActionItemKey(
    val team: Int,
    val iteration: String,
    val observation: Int,
    val actionItem: Int
)

@Serializable
data class ActionItem(
    val id: Int,
    val submittedBy: String,
    val observationKey: ObservationKey,
    val observationText: String,
    var text: String,
    var duration: Int = 1,
    var deleted: Boolean,
    val votes: MutableMap<String, Int>,
    val volunteers: MutableSet<String>,
    val ratings: MutableMap<String, Int>
)


@Serializable
data class CreateActionItemRequest(
    val text: String
)

@Serializable
data class CreateActionItemResponse(
    val id: Int
)

@Serializable
data class UpdateActionItemRequest(
    val text: String?,
    val duration: Int?
)

