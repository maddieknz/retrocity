package site.retrocity.common.model

import kotlinx.serialization.Serializable

@Serializable
data class ObservationKey(
    val team: Int,
    val iteration: String,
    val observation: Int
)

@Serializable
data class ObservationGroupKey(
    val team: Int,
    val iteration: String,
    val group: Int
)

@Serializable
data class Observation(
    val submittedBy: String,
    val group: Int,
    var text: String,
    var deleted: Boolean,
    val votes: MutableMap<String, Int>,
    var selected: Boolean,
    val actionItems: MutableList<Int>
)

@Serializable
data class ObservationGroup(
    val title: String,
    val votesPerUser: Int
)


@Serializable
data class CreateObservationRequest(
    val text: String,
    val group: Int
)

@Serializable
data class CreateObservationResponse(
    val index: Int
)

@Serializable
data class UpdateObservationRequest(
    val text: String? = null,
    val selected: Boolean? = null
)


@Serializable
data class ListObservationResult(
    val groups: List<Group>
) {

    @Serializable
    data class Group(
        val title: String,
        val observations: List<Observation>
    )

    @Serializable
    data class Observation(
        val index: Int,
        val text: String,
        val votes: Int,
        val selected: Boolean,
        val actions: List<Action>
    )

    @Serializable
    data class Action(
        val id: Int,
        val text: String,
        val votes: Int,
        val duration: Int
    )

}




