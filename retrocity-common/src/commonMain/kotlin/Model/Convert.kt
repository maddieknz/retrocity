package site.retrocity.common.model

fun Retrospective.toCreateRetrospectiveResponse() = CreateRetrospectiveResponse(
        actionItems = actionItems,
        observationGroups = observationGroups,
        participants = participants.toSet())

fun Retrospective.toViewRetrospectiveResponse() = ViewRetrospectiveResponse(
        team = team,
        iteration = iteration,
        state = state,
        stateStart = stateStart,
        owner = owner,
        participants = participants.toSet(),
        observationGroups = observationGroups,
        metrics =  sprintRatings.map { it.metric })

fun Retrospective.toListSpringRatingsResponse() = ListSprintRatingsResponse(
        metrics = sprintRatings.map { r ->
            val sum = r.ratings.values.sum()
            val size = r.ratings.size
            ListSprintRatingsResponse.Metric(
                    title = r.metric,
                    min = r.ratings.minBy { it.value }?.value ?: 0,
                    max = r.ratings.minBy { it.value }?.value ?: 0,
                    avg = if (size == 0) 0 else sum / size + if (size > 1 && sum % size >= size / 2) 1 else 0
            )
        })

fun Retrospective.toListSpringDescriptionsResponse() =
        sprintDescription.values.map { SprintDescriptionResponse(
            description = it.description,
            votes = it.votes.values.sum()) }

fun Retrospective.toListObservationResults(actionItems: Map<Int, ActionItem>) =
        ListObservationResult(
                groups = observationGroups.mapIndexed { groupIdx, group ->
                    ListObservationResult.Group(
                            title = group.title,
                            observations = observations.mapIndexedNotNull { oIdx, o ->
                                if (o.deleted || o.group != groupIdx)
                                    null
                                else
                                    ListObservationResult.Observation(
                                            index = oIdx,
                                            text = o.text,
                                            votes = o.votes.values.sum(),
                                            selected = o.selected,
                                            actions = o.actionItems.mapNotNull { actionItems[it] }
                                                    .map {
                                                        ListObservationResult.Action(
                                                                id = it.id,
                                                                duration = it.duration,
                                                                text = it.text,
                                                                votes = it.votes.values.sum()
                                                        )
                                                    }
                                    )
                            }
                    )
                }
        )