@file:UseSerializers(WDateTimeSerializer::class, WDateSerializer::class)
@file:OptIn(KlockExperimental::class)

package site.retrocity.common.model

import com.soywiz.klock.annotations.KlockExperimental
import com.soywiz.klock.wrapped.WDateTime
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable
data class RetrospectiveKey(
    val team: Int,
    val iteration: String
)

@Serializable
data class Retrospective(
    val team: Int,
    val iteration: Iteration,
    val owner: String,
    var state: RetroState,
    var stateStart: WDateTime,
    val participants: Set<String>,
    val actionItems: List<ActionItem>,
    val sprintDescription: MutableMap<String, SprintDescription>,
    val sprintRatings: List<SprintRating>,
    val observationGroups: List<ObservationGroup>,
    val observations: MutableList<Observation>
)

@Serializable
data class ViewRetrospectiveResponse(
    val team: Int,
    val iteration: Iteration,
    val owner: String,
    var state: RetroState,
    var stateStart: WDateTime,
    val participants: Set<String>,
    val observationGroups: List<ObservationGroup>,
    val metrics: List<String>
)

@Serializable
data class CreateRetrospectiveRequest(
    val team: Int,
    val iteration: String
)

@Serializable
data class CreateRetrospectiveResponse(
    val actionItems: List<ActionItem>,
    val participants: Set<String>,
    val observationGroups: List<ObservationGroup>
)

@Serializable
data class DescriptionRequest(
    val description: String
)

@Serializable
data class VoteRequest(
    val votes: List<Int>
)

@Serializable
data class RatingRequest(
    val rating: Int
)

@Serializable
enum class RetroState {
    RETRO_INIT,
    RETRO_STARTED,
    ACTION_ITEMS_RATE,
    SPRINT_EVALUATION,
    SPRINT_VOTING,
    SPRINT_REVIEW,
    OBSERVATIONS_SUBMIT,
    OBSERVATIONS_CLEANUP,
    OBSERVATIONS_VOTE,
    OBSERVATIONS_SELECT,
    ACTIONS_SUBMIT,
    ACTIONS_CLEANUP,
    ACTIONS_VOTE,
    ACTIONS_VOLUNTEER,
    RETRO_REVIEW
}

@Serializable
data class SprintDescription(
    val description: String,
    val votes: MutableMap<String, Int> = mutableMapOf()
)

@Serializable
data class SprintDescriptionResponse(
    val description: String,
    val votes: Int
)

@Serializable
data class SprintRating(
    val metric: String,
    val ratings: MutableMap<String, Int>
)


@Serializable
data class ListSprintRatingsResponse(
    val metrics: List<Metric>
) {

    @Serializable
    data class Metric(
        val title: String,
        val min: Int,
        val max: Int,
        val avg: Int
    )
}

@Serializable
data class VotesRequest(
    val votes: Map<Int, Int>
)
